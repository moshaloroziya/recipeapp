package com.example.recipeapp.Model

class Post {


    private var steps: String = ""
    private var postimage: String = ""
    private var engredients : String = ""
    private var description: String = ""
    private var category: String = ""
    private var postid: String = ""

    constructor()



    constructor(steps: String, postimage: String, engredients : String, description: String, category: String) {

        this.steps = steps
        this.postimage = postimage
        this.engredients = engredients
        this.description = description
        this.category = category
        this.postid = postid
    }


    fun getSteps(): String {
        return steps
    }

    fun getPostimage(): String {
        return postimage
    }

    fun getEngredients() : String {
        return engredients
    }

    fun getDescription(): String {
        return description
    }

    fun getCategory(): String{
        return category
    }
    fun getPostid(): String {
        return postid
    }


    fun setSteps(steps: String)
    {
        this.steps = steps
    }

    fun setPostimage(postimage: String)
    {
        this.postimage = postimage
    }

    fun setEngredients (engredients : String)
    {
        this.engredients  = engredients
    }

    fun setDescription(description: String)
    {
        this.description = description
    }
    fun setCategory(category: String){

        this.category = category
    }
    fun setPostid(postid: String)
    {
        this.postid = postid
    }
}