package com.example.recipeapp.Activity

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.example.recipeapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_recipe.*

class RecipeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe)

        val intent = getIntent();
        val RecypeName = intent.getStringExtra("name")
        val RecypeType = intent.getStringExtra("category")
        val RecypeSteps = intent.getStringExtra("steps")
        val RecypeEngredients = intent.getStringExtra("ingredients")
        val RecypeImage = intent.getStringExtra("imgUrl")
        val RecypeId = intent.getStringExtra("postid")


        DisplayRecipy(RecypeName, RecypeEngredients, RecypeImage, RecypeSteps )

        edit_post_btn.setOnClickListener {EnableView()}

        save_post_btn_recipe.setOnClickListener { saveChanges(RecypeImage, RecypeName,RecypeId, RecypeType)}

        delete_add_post_btn.setOnClickListener {deleteRecype(RecypeId, RecypeType)}

    }


    private fun deleteRecype(recypeId: String?, recypeType: String?) {

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Delete Post")
        builder.setMessage("are you sure to delete this Recipe?")

        builder.setPositiveButton("YES"){dialog, which ->

            FirebaseDatabase.getInstance().reference
                .child(recypeType.toString())
                .child(recypeId.toString())
                .removeValue()

            val intent = Intent(this,
                MainActivity::class.java)
            startActivity(intent)
        }

        builder.setNegativeButton("No"){dialog,which ->

        }

        val dialog: AlertDialog = builder.create()
        dialog.show()


    }

    private fun EnableView() {

        description_post_recipe.isEnabled = true
        engredients_post_recipe.isEnabled = true
        steps_post_recipe.isEnabled = true
        Toast.makeText(this,"Editing is Available", Toast.LENGTH_LONG).show()
        edit_post_btn.visibility = View.GONE
        save_post_btn_recipe.visibility = View.VISIBLE

    }

    private fun DisplayRecipy(recypeName: String?, recypeEngredients: String?, recypeImage: String?, recypeSteps: String?) {


        Picasso.get().load(recypeImage).into(image_post_recipe)
        description_post_recipe.setText(recypeName)
        engredients_post_recipe.setText(recypeEngredients)
        steps_post_recipe.setText(recypeSteps)

        DisableView()
    }

    private fun DisableView() {

        description_post_recipe.isEnabled = false
        engredients_post_recipe.isEnabled = false
        steps_post_recipe.isEnabled = false

        edit_post_btn.visibility = View.VISIBLE
        save_post_btn_recipe.visibility = View.GONE

    }

    private fun saveChanges(imgUrl: String, description: String,recypeId: String, recypeType: String) {


        when{
            TextUtils.isEmpty(description_post_recipe.text)->Toast.makeText(this,"Please enter food name",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(engredients_post_recipe.text)->Toast.makeText(this,"Please enter ingredients",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(steps_post_recipe.text)->Toast.makeText(this,"Please enter food cooking steps",Toast.LENGTH_LONG).show()

            else ->{

                val userRef = FirebaseDatabase.getInstance().reference.child(recypeType)

                val userMap = HashMap<String,Any>()


                userMap["category"] = recypeType
                userMap["description"] = description_post_recipe.text.toString()
                userMap["engredients"] = engredients_post_recipe.text.toString()
                userMap["steps"] = steps_post_recipe.text.toString()
                userMap["postimage"] = imgUrl
                userMap["userid"] = recypeId

                userRef.child(recypeId).updateChildren(userMap)

                Toast.makeText(this,"Recipe has been updated succesfully", Toast.LENGTH_LONG).show()


                 DisableView()


            }

        }


    }
}