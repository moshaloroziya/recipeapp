package com.example.recipeapp.Activity

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.recipeapp.R
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_add_post.*
import kotlinx.android.synthetic.main.activity_recipe.*

class AddPostActivity : AppCompatActivity() {
    private  var myUrl = ""
    private var  imageUri: Uri? = null
    private var  storagePostPicRef: StorageReference? = null
    lateinit var RecipeTypeSpinner: Spinner
    private var SpinnerSelectedText: String? = null

    private lateinit var auth: FirebaseAuth

    private  var TAG = "TAG"




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)

        RecipeTypeSpinner = findViewById(R.id.category_spinner)



        auth = FirebaseAuth.getInstance()

        storagePostPicRef = FirebaseStorage.getInstance().reference.child("Recipes Pictures")

        save_new_post_btn.setOnClickListener{ uploadImage() }

        close_add_post_btn.setOnClickListener{ callIntent()}

        image_post.setOnClickListener {

            CropImage.activity()
           .setAspectRatio(2,1)
           .start(this@AddPostActivity)

        }

        val recipeTypes = arrayOf("Sweats", "Soups", "Spicy", "Seafood","Noodles")

        RecipeTypeSpinner.adapter = this?.let {
            ArrayAdapter<String>(
                it,
                android.R.layout.simple_list_item_1,
                recipeTypes
            )
        }

        RecipeTypeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                SpinnerSelectedText = recipeTypes[position]

            }
        }
    }

    private fun callIntent() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {

            val result = CropImage.getActivityResult(data)
            imageUri = result.uri
            image_post.setImageURI(imageUri)


        }
    }
    private fun uploadImage() {

        when{

            imageUri == null-> Toast.makeText(this,"Please select Image for your recipe", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(description_post.text)->Toast.makeText(this,"Please enter food name",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(engredients_post.text)->Toast.makeText(this,"Please enter ingredients",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(steps_post.text)->Toast.makeText(this,"Please enter food cooking steps",Toast.LENGTH_LONG).show()

            else->{

                val progressDialog = ProgressDialog(this)
                progressDialog.setTitle("Uploading New Recipe")
                progressDialog.setMessage("Please Wait...")
                progressDialog.show()

                val fileRef = storagePostPicRef!!.child(System.currentTimeMillis().toString() + ".jpg")

                var uploadTask: StorageTask<*>
                uploadTask = fileRef.putFile(imageUri!!)

                uploadTask.continueWithTask(Continuation <UploadTask.TaskSnapshot, Task<Uri>> { task ->

                    if(!task.isSuccessful){

                        task.exception?.let{
                            throw it
                            progressDialog.dismiss()
                        }
                    }

                    return@Continuation  fileRef.downloadUrl

                }).addOnCompleteListener(

                    OnCompleteListener<Uri> {task ->

                        if(task.isSuccessful){

                            val downloadUrl = task.result
                            myUrl = downloadUrl.toString()

                            val ref = FirebaseDatabase.getInstance().reference.child(SpinnerSelectedText!!)

                            val postId = ref.push().key

                            val postMap = HashMap<String,Any>()
                           // postMap["postid"] = postId!!
                            postMap["category"] = SpinnerSelectedText.toString()
                            postMap["description"] = description_post.text.toString()
                            postMap["engredients"] = engredients_post.text.toString()
                            postMap["steps"] = steps_post.text.toString()
                            postMap["postimage"] = myUrl
                            postMap["postid"] = postId.toString()






                            ref.child(postId.toString()).updateChildren(postMap)

                            Toast.makeText(this,"Your Recipe Updated Succesfully", Toast.LENGTH_LONG).show()

                            val intent = Intent(this@AddPostActivity,
                                MainActivity::class.java)
                            startActivity(intent)
                            finish()

                            progressDialog.dismiss()
                        }else{
                            progressDialog.dismiss()
                        }

                    })
            }
        }

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.

        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?) {

        auth.signInAnonymously()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInAnonymously:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInAnonymously:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }

                // ...
            }

    }




}