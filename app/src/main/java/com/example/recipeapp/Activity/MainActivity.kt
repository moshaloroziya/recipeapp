package com.example.recipeapp.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recipeapp.Adapter.PostAdapter
import com.example.recipeapp.Model.Post
import com.example.recipeapp.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.recipetypes.*

class MainActivity : AppCompatActivity() {

    lateinit var RecipeTypeSpinner: Spinner
    private var postAdapter: PostAdapter? = null
    private var SpinnerSelectedText: String? = null
    private var postList: MutableList<Post>? = null
    var recyclerView: RecyclerView? = null
    private var SpinnertSelected: String? = null
    private var mPost: MutableList<Post>?= null
    private  var TAG = "TAG"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.recipetypes)

        RecipeTypeSpinner = findViewById(R.id.spinner_id)

        recyclerView = findViewById(R.id.recycler_view)
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.reverseLayout = true
        linearLayoutManager.stackFromEnd = true
        recyclerView!!.layoutManager = linearLayoutManager

        postList = ArrayList()

        postAdapter = this?.let{
            PostAdapter(
                it,
                postList as ArrayList<Post>
            )
        }
        recyclerView!!.adapter = postAdapter


        search_text_id.addTextChangedListener(object: TextWatcher {


            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}


            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                retrieveUsers()
                searchUser(s.toString())


            }

            override fun afterTextChanged(s: Editable?) {

            }


        })




        options_view!!.setOnClickListener{
            startActivity(Intent(this, AddPostActivity::class.java))
        }

        val recipeTypes = arrayOf("Sweats", "Soups", "Spicy", "Seafood","Noodles")

        RecipeTypeSpinner.adapter = this?.let {
            ArrayAdapter<String>(
                it,
                android.R.layout.simple_list_item_1,
                recipeTypes
            )
        }

        RecipeTypeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                SpinnerSelectedText = recipeTypes[position]

                retrievePosts()

            }

        }

    }
    private fun retrievePosts() {


        val postsRef = FirebaseDatabase.getInstance().reference.child(SpinnerSelectedText.toString())

        postsRef.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(p0: DataSnapshot) {

                postList?.clear()

                for(snapshot in p0.children){
                    val post = snapshot.getValue(Post::class.java)

                    postList!!.add(post!!)
                    postAdapter!!.notifyDataSetChanged()
                }


            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })

    }


    private fun searchUser(input: String)
    {
        val query = FirebaseDatabase.getInstance().getReference()
            .child(SpinnerSelectedText!!)
            .orderByChild("description")
            .startAt(input)
            .endAt(input + "\uf8ff" )

        query.addValueEventListener(object : ValueEventListener{

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                postList?.clear()
                for (snapshot in dataSnapshot.children)

                {
                    val post = snapshot.getValue(Post::class.java)
                    if(post != null){
                        postList?.add(post)
                    }
                }
                postAdapter?.notifyDataSetChanged()

            }

            override fun onCancelled(p0: DatabaseError) {
            }

        })

    }

    private fun retrieveUsers() {


        val usersRef = FirebaseDatabase.getInstance().getReference().child(SpinnerSelectedText!!)

        usersRef.addValueEventListener(object : ValueEventListener{

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(search_text_id?.text.toString() == ""){

                    postList?.clear()
                    for (snapshot in dataSnapshot.children)

                    {
                        val post = snapshot.getValue(Post::class.java)
                        if(post != null){

                            postList?.add(post)

                        }
                    }
                    postAdapter?.notifyDataSetChanged()

                    if(postAdapter!!.itemCount == 0){

                        Toast.makeText(this@MainActivity,"Such Recipe doesn't exist",Toast.LENGTH_LONG).show()

                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {

            }

        })
    }
}