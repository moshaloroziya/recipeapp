package com.example.recipeapp.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.example.recipeapp.Model.Post
import com.example.recipeapp.R
import com.example.recipeapp.Activity.RecipeActivity

import com.google.firebase.auth.FirebaseUser
import com.squareup.picasso.Picasso


class PostAdapter
    (private val mContext: Context,
     private val mPost: List<Post>) : RecyclerView.Adapter<PostAdapter.ViewHolder>()
{
    private var firebaseUser: FirebaseUser? = null
    var counter = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(mContext).inflate(R.layout.recipes_layout_item,parent,false)

        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mPost.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //firebaseUser = FirebaseAuth.getInstance().currentUser

        val post = mPost[position]

        Picasso.get().load(post.getPostimage()).into(holder.postImage)
        holder.recipeName.text = post!!.getDescription()

        //displayRecipes(holder.recipeName)

        holder.postImage.setOnClickListener {

            val intent = Intent(mContext, RecipeActivity::class.java)
            intent.putExtra("name", post.getDescription())
            intent.putExtra("category", post.getCategory())
            intent.putExtra("imgUrl", post.getPostimage())
            intent.putExtra("steps",post.getSteps())
            intent.putExtra("ingredients", post.getEngredients())
            intent.putExtra("postid", post.getPostid())
            mContext.startActivity(intent)
        }


    }
    inner class ViewHolder(@NonNull itemView: View): RecyclerView.ViewHolder(itemView)
    {

       var postImage: ImageView
       var recipeName: TextView


        init {
           recipeName = itemView.findViewById(R.id.recipe_name)
           postImage = itemView.findViewById(R.id.recipe_img)


       }
    }
}